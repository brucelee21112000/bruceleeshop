﻿using Entity.Context;
using Entity.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Model.Response;
using Model.UserInfo;
using Repository.Account;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace BruceLeeApi.Controllers
{
    [Route("api")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IAccountRepository _accountRepository;
        private readonly BruceLeeDbContext _context;

        public AccountsController(IConfiguration configuration, IAccountRepository accountRepository, BruceLeeDbContext context)
        {
            _accountRepository = accountRepository;
            _context = context;
            _configuration = configuration;
        }

        [Route("Register")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<Response<User>> Register([FromBody] RegisterModel request)
        {
            return await _accountRepository.Register(request);
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Authenticate(LoginModel model)
        {
            var result = await _accountRepository.Login(model);

            if (result.Code == StatusCodes.Status200OK)
            {
                var jwt = _configuration.GetSection("Jwt").Get<Jwt>();

                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, jwt.Subject),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                    new Claim("UserName", model.UserName),
                    new Claim("Password", model.Password)
                };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwt.key));

                var signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(
                            jwt.Issuer,
                            jwt.Audience,
                            claims,
                            expires: DateTime.Now.AddMinutes(20),
                            signingCredentials: signIn
                    );

                return Ok(new JwtSecurityTokenHandler().WriteToken(token));
            }
            else
            {
                return BadRequest("Authenticate Failed !");
            }
        }
    }
}