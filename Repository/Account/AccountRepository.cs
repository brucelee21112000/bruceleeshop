﻿using Entity.Context;
using Entity.Entity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Model.Response;
using Model.UserInfo;

namespace Repository.Account
{
    public class AccountRepository : Repository<AccountRepository>, IAccountRepository
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IConfiguration _configuration;

        public AccountRepository(
            UserManager<User> userManager,
            IConfiguration configuration,
            BruceLeeDbContext context,
            SignInManager<User> signInManager
            ) : base(context)
        {
            _userManager = userManager;
            _configuration = configuration;
            _signInManager = signInManager;
        }

        public async Task<Response<User>> Register(RegisterModel model)
        {
            Response<User> res = new Response<User>();
            try
            {
                var user = new User()
                {
                    UserName = model.UserName,
                    FullName = model.FullName,
                    PasswordHash = model.Password
                };
                var result = await _userManager.CreateAsync(user, user.PasswordHash);
                if (result.Succeeded)
                {
                    res.Code = StatusCodes.Status200OK;
                    res.Message = "Register succesfull !";
                }
                else
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Register failed !";
                }
            }
            catch (Exception)
            {
                res.Code = StatusCodes.Status500InternalServerError;
            }
            return res;
        }

        public async Task<Response<User>> Login(LoginModel model)
        {
            Response<User> res = new Response<User>();
            try
            {
                var user = await _context.Users.Where(x => x.UserName == model.UserName && x.IsDelete == false).FirstOrDefaultAsync();
                if (user == null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Tài khoản không tồn tại!";
                    res.Data = user;
                    return res;
                }
                else
                {
                    var verigyResult = _userManager.PasswordHasher.VerifyHashedPassword(user, user.PasswordHash, model.Password);
                    if (verigyResult != PasswordVerificationResult.Failed) // either verigyResult equal to SuccessRehashNeeded or Success
                    {
                        res.Code = StatusCodes.Status200OK;
                        res.Message = "Đăng nhập thành công!";
                        res.Data = user;
                        return res;
                    }
                    else
                    {
                        res.Code = StatusCodes.Status404NotFound;
                        res.Message = "Tài khoản hoặc mật khẩu bị sai!";
                        res.Data = user;
                        return res;
                    }
                }
            }
            catch (Exception)
            {
                res.Code = StatusCodes.Status500InternalServerError;
            }
            return res;
        }
    }

    public class Jwt
    {
        public string? key { get; set; }
        public string? Issuer { get; set; }
        public string? Audience { get; set; }
        public string? Subject { get; set; }
    }
}