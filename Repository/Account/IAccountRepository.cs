﻿using Entity.Entity;
using Model.Response;
using Model.UserInfo;

namespace Repository.Account
{
    public interface IAccountRepository
    {
        Task<Response<User>> Register(RegisterModel model);

        Task<Response<User>> Login(LoginModel model);
    }
}